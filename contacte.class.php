<?php
    class contacte{
        private $id;
        private $nom;
        private $telefon;
        private static $contadorids = -1;

        public function __construct($id, $nom, $telefon){
            $this->id = $id;
            $this->nom = $nom;
            $this->telefon = $telefon;
        }
        public function getId(){
            return $this->id;
        }
        public function getNom(){
            return $this->nom;
        }
        public function getTelefon(){
            return $this->telefon;
        }
        public function setId($id){
            $this->id = $id;
        }
        public function setNom($nom){
            $this->nom = $nom;
        }
        public function setTelefon($telefon){
            $this->telefon = $telefon;
        }

        public function mostrar(){
            echo '<p> id: "' . $this->id . '"</p>';
            echo '<p> nom: "' . $this->nom . '"</p>';
            echo '<p> telefon: ' . $this->telefon . '</p>'; 
        }

        public function __clone(){
            $this->nom = "CLON DE $this->nom";
            $this->id = self::$contadorids;
            self::$contadorids--;
        }
    }
?>